# Tokyo Debian Apt JSON Hook 202311

## How to show

  % rabbit

## How to install

  % gem install rabbit-slide-kenhys-tokyodebian-apt-json-hook-202311

## How to create PDF

  % rake pdf


# References

https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-apt-json-hook-202309

https://www.clear-code.com/blog/2023/10/19/use-apt-json-hook.html

https://gihyo.jp/admin/serial/01/ubuntu-recipe/0676

https://salsa.debian.org/apt-team/apt/-/blob/main/doc/json-hooks-protocol.md

https://salsa.debian.org/apt-team/apt/-/blob/main/test/integration/test-apt-cli-json-hooks#L29-67

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1051271

https://wiki.debian.org/UltimateDebianDatabase
